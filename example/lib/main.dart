import 'package:flutter/material.dart';
import 'components/payment_kaliapay.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kaliapay',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

      initialRoute:  '/' ,  
      routes : {  
      '/' : (context) => FormPage(),  
      // '/connexion' : (context) => LoginKaliaPay(),  
      },  
    );
  }
}

