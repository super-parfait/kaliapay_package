import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kaliapay/kaliapay.dart';
import 'package:url_launcher/url_launcher.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {

  final _formKey = GlobalKey<FormState>();
  final  _formKey2 = GlobalKey<FormState>();
  String reference = '';
  List<Map<String, dynamic>> response = [];


  final TextEditingController  userController = TextEditingController();
  final TextEditingController  passwordController = TextEditingController();


  final TextEditingController _apikeyController = TextEditingController();
  final TextEditingController _amountController = TextEditingController();
  final TextEditingController _serviceController = TextEditingController();
  final TextEditingController _custom_dataController = TextEditingController();

    // Ma fonction pour ouvrir le formulaire de connexion de KaliaPay
    Future<void> showInformationDialog(BuildContext context) async {
      return await showDialog(
        context: context,
        builder: (context) {
          // bool isChecked = false;
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: Form(
                  key: _formKey2,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        controller: userController,
                        validator: (value) {
                          if (value!.isNotEmpty) {
                            return null;
                          } else {
                            return "Email ou nom d'utilisateur ";
                          }
                        },
                        decoration:
                            InputDecoration(hintText: "Veuillez entrer vos identifiants"),
                      ),
                      TextFormField(
                        controller: passwordController,
                        validator: (value) {
                          if (value!.isNotEmpty) {
                            return null;
                          } else {
                            return "Mot de passe";
                          }
                        },
                        decoration:
                            InputDecoration(hintText: "Veuillez entrer le mot de passe"),
                      ),
                      
                    ],
                  )
                ),
              title: Text('Commencer le paiement'),
              actions: <Widget>[
                InkWell(
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.blue, // Couleur du bouton
                    ),
                    child: const Text(
                      'Valider',
                      style: TextStyle(
                        color: Colors.white, // Couleur du texte
                        fontSize: 16,
                      ),
                    ),
                  ),
                  onTap: () async {
                    
                    if (_formKey2.currentState!.validate()) {
                      // JE pourrais gerer le localstorage ici
                      String user = userController.text.trim();
                      String password =  passwordController.text;
                      var response = await KaliapayCheckout().loginUser(user, password);
                      print(response);
                      Navigator.of(context).pop();
                    }
                  },
                ),
              ],
            );
          }
        );
        }
      );
    }

    // Ma fonction pour ouvrir KaliaPay via une webview
    Future<void> _launchUrl(Uri url) async {
      if (await canLaunchUrl(url)){
            
        await launchUrl(
          url,
          mode: LaunchMode.inAppWebView,
          webViewConfiguration: const WebViewConfiguration(
                  headers: <String, String>{'KALIAPAY': 'KALIAPAY_PAYMENT'}
          ),
        );
      } 
    }

    // Ma fonction pour recuperer la liste des transactions 
    void fetchData(String reference) async {
        // print('cpoucou'+reference);
        var details = await KaliapayCheckout().GetDetailsPayment(reference);
        
        print(details);


        setState(() {
          response.add(details) ;
        });
    }


    // @override
    // void initState() {
    //   super.initState();
    //   // _timer = Timer.periodic(const Duration(seconds: 3), (timer) {
    //   //   // Appeler votre fonction ici
    //   //   fetchData(reference);
    //   // });
     
    // }

    


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleTextStyle: const TextStyle(color: Colors.amber,fontSize: 30, fontWeight: FontWeight.bold),
        title: const Text(
          "Package KaliaPay",
          textAlign: TextAlign.center,
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.account_box),
              tooltip: 'Se connecter',
              onPressed: () async {
                await showInformationDialog(context);
              },
            ),
          ],
      ),
      body: SingleChildScrollView(
        
        child: Column(
          children: [
            Center(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CustomFormField(
                      controller: _apikeyController,
                      hintText: 'votre apikey',
                    ),
                    CustomFormField(
                      controller: _serviceController,
                      hintText: 'votre service',
                    ),
                    CustomFormField(
                      controller: _amountController,
                      hintText: 'votre montant',
                    ),
                    CustomFormField(
                      controller: _custom_dataController,
                      hintText: 'le custom_data',
                    ),
                    ElevatedButton(
                      onPressed: ()async  {
                        if (_formKey.currentState!.validate()) {

                          String apikey = _apikeyController.text;
                          String service = _serviceController.text;
                          int amount = int.parse(_amountController.text);
                          String custom_data = _custom_dataController.text;

                          print('voila mon apikey $apikey, $service, $amount, $custom_data');

                          // Effectuer les actions nécessaires avec les données saisies
                          await KaliapayCheckout().CreatePayment( apikey, service,amount, custom_data).then((data){
                            // Si vous souhaitez afficher un code QR 
                            // String qrcode_url= data['qrcode_url'];

                            // Si vous voulez etre rediriger sur la page de paiement directement
                            String shortened_url = data['shortened_url'];
                            Uri url = Uri.parse(shortened_url);
                            
                            // Je lance la webview dans un Launcher
                            _launchUrl(url);
                            
                            setState(() {
                              reference =  data['reference'];
                            });
                          });
                          // Réinitialisez le formulaire
                          // _formKey.currentState!.reset();
                        }
                      },
                      child: const Text(
                        'Payer',
                        style: TextStyle(fontSize: 50),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Column(
              children: [
                  ElevatedButton(onPressed: (){ fetchData(reference);}, child: const Text('Liste de Transactions')),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: response.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text('Reference: ${response[index]['reference']}'),
                        subtitle: Text('Status: ${response[index]['status']}'),
                      );
                    },
                  ),
              ],
            )

            // FutureBuilder<String>(
            //   future: fetchData(reference),
            //   builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            //     if (snapshot.connectionState == ConnectionState.waiting) {
            //       return CircularProgressIndicator();
            //     } else {
            //       return Text('Résultat: ${snapshot.data}');
            //     }
            //   },
            // ),
            
          ],
        ),
      ),
    );
  }
}

class CustomFormField extends StatelessWidget {
  CustomFormField({Key? key, required this.hintText, this.controller,
    }) : super(key: key);

    final String hintText;
    final controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: TextFormField(

          controller: controller,
          decoration: InputDecoration(
          hintText: hintText,
          enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
          width: 3, color: Colors.blue),
          
        ),),
      ),
    );
  }
}