import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kaliapay/kaliapay.dart';


class LoginKaliaPay extends StatefulWidget {
  @override
  _LoginKaliaPayState createState() => _LoginKaliaPayState();
}

class _LoginKaliaPayState extends State<LoginKaliaPay> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController  userController = TextEditingController();
  final TextEditingController  passwordController = TextEditingController();

  Future<String?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = await prefs.getString('token');

    return token;
  }

  Future<void> showInformationDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          // bool isChecked = false;
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        controller: userController,
                        validator: (value) {
                          if (value!.isNotEmpty) {
                            return null;
                          } else {
                            return "Email ou nom d'utilisateur ";
                          }
                        },
                        decoration:
                            InputDecoration(hintText: "Veuillez entrer vos identifiants"),
                      ),
                      TextFormField(
                        controller: passwordController,
                        validator: (value) {
                          if (value!.isNotEmpty) {
                            return null;
                          } else {
                            return "Mot de passe";
                          }
                        },
                        decoration:
                            InputDecoration(hintText: "Veuillez entrer le mot de passe"),
                      ),
                      
                    ],
                  )
                ),
              title: Text('Commencer le paiement'),
              actions: <Widget>[
                InkWell(
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.blue, // Couleur du bouton
                    ),
                    child: const Text(
                      'Valider',
                      style: TextStyle(
                        color: Colors.white, // Couleur du texte
                        fontSize: 16,
                      ),
                    ),
                  ),
                  onTap: () async {
                    
                    if (_formKey.currentState!.validate()) {
                      // JE pourrais gerer le localstorage ici
                      String user = userController.text.trim();
                      String password =  passwordController.text;
                      await KaliapayCheckout().loginUser(user, password).then((response) => {
                        print("La reponse est: "+response.toString())
                      });
                      
                      Navigator.of(context).pop();
                    }
                  },
                ),
              ],
            );
          }
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: ElevatedButton(
              // color: Colors.deepOrange,
            onPressed: () async {
              await showInformationDialog(context);
            },
            child: Text(
              "Connexion Kaliapay",
              style: TextStyle(color: Colors.white, fontSize: 16),
            )
          ),
        ),
      ),
    );
  }
}

