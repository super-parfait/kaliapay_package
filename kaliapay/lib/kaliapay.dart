library kaliapay;

// export 'package:kaliapay/paiement/kaliapayCheckout.dart';
// export 'package:kaliapay/paiement/webViewKaliapay.dart';
// export 'package:kaliapay/paiement/webView.dart';

// import 'dart:convert';
// import 'package:get/get.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';





class KaliapayCheckout{

  
  Dio dio = Dio();
  var paymentUrl =("https://kaliapay.com/api/generate-mobpay-qrcode/");
  var loginUrl = ('https://kaliapay.com/api/signin-users/');
  var detailPayment = ("https://kaliapay.com/api/get-express-transaction-details/");

  // La fonction pour la connexion d'un marchand
  Future loginUser(dynamic user, dynamic password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = {
      'user': user,
      'password': password,
    };

    final response = await dio.post(
      loginUrl,
      data: data,
      options: Options(
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        }
      )
    );

    if (response.statusCode == 200) {

      var result = response.data['result'];
      String token = result['tid'];
      prefs.setString('token', token);
      print("Le token est: "+token);
      return result;

    } else {

      return "Erreur de connexion...";

    }
  }

    
  // La fonction pour initialiser le paiement
  Future CreatePayment(String apikey, String service,int amount, String custom_data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token').toString();

    final data ={
      'apikey': apikey,
      'service': service,
      'amount': amount.toInt(),
      'custom_data': custom_data,
    };

    print("Voila mes data $data");


    final response = await dio.post(
      paymentUrl,
      data: data,
      options: Options(
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Token $token',
        }
      )
    );


    if (response.statusCode == 200) {

      var result = response.data['result'];
      return result;

    } else {

      return "Erreur lors de la transaction";

    }
  }

  // La fonction pour recuperer les details d'un paiement
  Future GetDetailsPayment(String reference) async {
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token').toString();
    final response = await dio.get(
      detailPayment+reference,
      options: Options(
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Token $token',
        }
      )
    );


    if (response.statusCode == 200) {

      var result = response.data['result'];
      return result;

    } else {

      return "Erreur lors de la recuperation des détails";

    }

  }

}